// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "TDS/Components/CharacterAnimationComponent.h"
#include "TDS/FuncLibrary/Types.h"
#include "CoreAnimInstance.generated.h"

class UCharacterMovementComponent;
/**
 * 
 */
UCLASS()
class TDS_API UCoreAnimInstance : public UAnimInstance
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadOnly, Category = "AnimCharacter|Components")
	ACharacter* OwningCharacter;

	UPROPERTY(BlueprintReadOnly, Category = "AnimCharacter|Components")
	UCharacterMovementComponent* OwnerMovementComponent;

	UPROPERTY(BlueprintReadOnly, Category = "AnimCharacter|Components")
	UCharacterAnimationComponent* AnimComponent;

	UPROPERTY(BlueprintReadOnly, Category = "AnimCharacter|Anim States")
	FGameplayTagContainer ModificationTags;

	UPROPERTY(BlueprintReadOnly, Category = "AnimCharacter|Anim States")
	FGameplayTag BasePose;

	UPROPERTY(BlueprintReadOnly, Category = "AnimCharacter|Anim States")
	FGameplayTag OverlayPose;

	UPROPERTY(BlueprintReadOnly, Category = "AnimCharacter|AimOffset")
	FRotator RawAimOffset;

	UPROPERTY(BlueprintReadOnly, Category = "AnimCharacter|AimOffset")
	EAimOffsets AimOffsetType;

	UPROPERTY(BlueprintReadOnly, Category = "AnimCharacter|AimOffset")
	EAimOffsetClamp AimOffsetBehavior;

	UPROPERTY(BlueprintReadOnly, Category = "AnimCharacter|AimOffset")
	FVector LookAtLocation;

	UPROPERTY(BlueprintReadOnly, Category = "AnimCharacter|AimOffset")
	FVector2D FinalAimOffset;

	UPROPERTY(BlueprintReadOnly, Category = "AnimCharacter|AimOffset")
	float AimClamp;

	UPROPERTY(BlueprintReadOnly, Category = "AnimCharacter|Rotation")
	ERotationMethod RotationMethod;

	UPROPERTY(BlueprintReadOnly, Category = "AnimCharacter|Runtime")
	float DeltaTick;

	UPROPERTY(BlueprintReadOnly, Category = "AnimCharacter|Runtime")
	FRotator PreviousRotation;

	UPROPERTY(BlueprintReadOnly, Category = "AnimCharacter|Movement")
	FRotator PreviousFrameAim;

	UPROPERTY(BlueprintReadOnly, Category = "AnimCharacter|Movement")
	float Velocity;

	UPROPERTY(BlueprintReadOnly, Category = "AnimCharacter|Movement")
	float ForwardVelocity;

	UPROPERTY(BlueprintReadOnly, Category = "AnimCharacter|Movement")
	float StrafeVelocity;

	UPROPERTY(BlueprintReadOnly, Category = "AnimCharacter|Movement")
	float UpVelocity;

	UPROPERTY(BlueprintReadOnly, Category = "AnimCharacter|Movement")
	float Direction;

	UPROPERTY(BlueprintReadOnly, Category = "AnimCharacter|Runtime")
	FVector InputAcceleration;

	UPROPERTY(BlueprintReadOnly, Category = "AnimCharacter|State")
	bool bIdle;

	UPROPERTY(BlueprintReadOnly, Category = "AnimCharacter|State")
	bool bInAir;

	UPROPERTY(BlueprintReadOnly, Category = "AnimCharacter|State")
	bool bStanding;

	UPROPERTY(BlueprintReadOnly, Category = "AnimCharacter|State")
	bool bSwimming;

	UPROPERTY(BlueprintReadOnly, Category = "AnimCharacter|State")
	bool bCrouching;

	UPROPERTY(BlueprintReadOnly, Category = "AnimCharacter|State")
	bool bGrounded;

	UPROPERTY(BlueprintReadOnly, Category = "AnimCharacter|State")
	bool bWalkingState;

	UPROPERTY(BlueprintReadOnly, Category = "AnimCharacter|State")
	bool bFlying;

	UPROPERTY(BlueprintReadOnly, Category = "AnimCharacter|State")
	bool bFalling;

	UPROPERTY(BlueprintReadOnly, Category = "AnimCharacter|State")
	TEnumAsByte<EMovementMode> MovementMode;

	UPROPERTY(BlueprintReadOnly, Category = "AnimCharacter|Leans")
	float Lean;

	UPROPERTY(BlueprintReadOnly, Category = "AnimCharacter|Leans")
	FVector2D AimDelta;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "AnimCharacter|Setup")
	float TargetFrameRate;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "AnimCharacter|Setup")
	float LeanSmooth;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "AnimCharacter|Setup")
	float AimSmooth;

private:
	float IdleXY;
	FVector VelocityVector;

public:
	UCoreAnimInstance(const FObjectInitializer& ObjectInitializer);

private:
	virtual void NativeInitializeAnimation() override;
	virtual void NativeUpdateAnimation(float DeltaSeconds) override;

	void RecastOwnerComponents();
	void GetComponentVariables();
	void SetMovementVectorsAndStates();
	void SetupLeaning();
	void SetupAimOffset();
	void SetupMovementStates();
	float NormalizeLean(const float InValue) const;
	bool IsStanding() const;
};
