// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS/Animations/CoreAnimInstance.h"
#include "TDS/FuncLibrary/FuncLib.h"
#include "GameFramework/Character.h"
#include "Kismet/KismetMathLibrary.h"

UCoreAnimInstance::UCoreAnimInstance(const FObjectInitializer& ObjectInitializer)
	: UAnimInstance(ObjectInitializer)
{
	TargetFrameRate = 60.0f;
	LeanSmooth = 6.0f;
	AimSmooth = 6.0f;
}

void UCoreAnimInstance::NativeInitializeAnimation()
{
	Super::NativeInitializeAnimation();
	RecastOwnerComponents();
}

void UCoreAnimInstance::NativeUpdateAnimation(float DeltaSeconds)
{
	Super::NativeUpdateAnimation(DeltaSeconds);
	DeltaTick = DeltaSeconds;

	GetComponentVariables();
	SetMovementVectorsAndStates();
	SetupLeaning();
	SetupAimOffset();
	SetupMovementStates();
}

void UCoreAnimInstance::RecastOwnerComponents()
{
	OwningCharacter = Cast<ACharacter>(TryGetPawnOwner());
	if(OwningCharacter)
	{
		OwnerMovementComponent = OwningCharacter->GetCharacterMovement();
		AnimComponent = UFuncLib::GetAnimationComponent(OwningCharacter);
	}
}

void UCoreAnimInstance::GetComponentVariables()
{
	if(AnimComponent)
	{
		ModificationTags = AnimComponent->AnimModTags;
		BasePose = AnimComponent->BasePose;
		OverlayPose = AnimComponent->OverlayPose;
		AimOffsetType = AnimComponent->AimOffsetType;
		AimOffsetBehavior = AnimComponent->AimOffsetBehavior;
		RawAimOffset = AnimComponent->AimOffset;
		RotationMethod = AnimComponent->RotationMethod;
		LookAtLocation = AnimComponent->LookAtLocation;
		AimClamp = AnimComponent->AimClamp;
	}
	else
	{
		RecastOwnerComponents();
	}
}

void UCoreAnimInstance::SetMovementVectorsAndStates()
{
	if(TryGetPawnOwner())
	{
		Velocity = TryGetPawnOwner()->GetVelocity().Size();
		VelocityVector = TryGetPawnOwner()->GetVelocity();
		const FRotator Rotation = TryGetPawnOwner()->GetActorRotation();

		UpVelocity = Rotation.UnrotateVector(TryGetPawnOwner()->GetVelocity()).Z;
		ForwardVelocity = Rotation.UnrotateVector(TryGetPawnOwner()->GetVelocity()).X;
		StrafeVelocity = Rotation.UnrotateVector(TryGetPawnOwner()->GetVelocity()).Y;
		IdleXY = FVector(VelocityVector.X, VelocityVector.Y, 0.0f).Size();

		bIdle = FMath::IsNearlyZero(IdleXY);
		if(!bIdle)
		{
			Direction = CalculateDirection(TryGetPawnOwner()->GetVelocity(), TryGetPawnOwner()->GetActorRotation());
		}
		if(OwnerMovementComponent)
		{
			InputAcceleration = OwnerMovementComponent->GetCurrentAcceleration();
		}
	}
}

void UCoreAnimInstance::SetupLeaning()
{
	if(TryGetPawnOwner())
	{
		const FRotator Delta = UKismetMathLibrary::NormalizedDeltaRotator(PreviousRotation, TryGetPawnOwner()->GetActorRotation());
		Lean = FMath::FInterpTo(Lean, NormalizeLean(Delta.Yaw),DeltaTick,LeanSmooth);
		PreviousRotation = TryGetPawnOwner()->GetActorRotation();
	}	
}

void UCoreAnimInstance::SetupAimOffset()
{
	if(!TryGetPawnOwner())
	{
		return;
	}

	FRotator Delta = UKismetMathLibrary::NormalizedDeltaRotator(RawAimOffset, TryGetPawnOwner()->GetActorRotation());
	const float Min = AimClamp * -1.0f;
	const float Max = AimClamp;

	float TargetX = TargetX = Delta.Yaw;
	const float TargetY = Delta.Pitch;

	if(!UKismetMathLibrary::InRange_FloatFloat(Delta.Yaw, Min, Max, true, true))
	{
		switch(AimOffsetBehavior)
		{
		case EAimOffsetClamp::Nearest:
			TargetX = FMath::Clamp(Delta.Yaw, Min, Max);
			break;

		case EAimOffsetClamp::Left:
			TargetX = Min;
			break;

		case EAimOffsetClamp::Right:
			TargetX = Max;
			break;

		default:
			checkNoEntry();
		}
	}

	FinalAimOffset = FMath::Vector2DInterpTo(FinalAimOffset, FVector2D(TargetX, TargetY),DeltaTick, AimSmooth);

	Delta = UKismetMathLibrary::NormalizedDeltaRotator(PreviousFrameAim,RawAimOffset);
	AimDelta = FMath::Vector2DInterpTo(AimDelta, FVector2D(NormalizeLean(Delta.Yaw), NormalizeLean(Delta.Pitch)), DeltaTick, LeanSmooth);
	PreviousFrameAim = RawAimOffset;
}

void UCoreAnimInstance::SetupMovementStates()
{
	if(OwnerMovementComponent)
	{
		bFalling = OwnerMovementComponent->IsFalling();
		bCrouching = OwnerMovementComponent->IsCrouching();
		bFlying = OwnerMovementComponent->IsFlying();
		bSwimming = OwnerMovementComponent->IsSwimming();
		bWalkingState = OwnerMovementComponent->IsWalking();
		bGrounded = OwnerMovementComponent->IsMovingOnGround();
		bInAir = bFalling || bFlying;
		MovementMode = OwnerMovementComponent->MovementMode;
	}

	bStanding = IsStanding();
}

float UCoreAnimInstance::NormalizeLean(const float InValue) const
{
	return  (InValue*(1.0/DeltaTick))/TargetFrameRate;	
}

bool UCoreAnimInstance::IsStanding() const
{
	return !bCrouching;
}
