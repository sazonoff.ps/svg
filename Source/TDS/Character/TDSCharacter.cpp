// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDSCharacter.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "TDS/Components/CharacterInventoryComponent.h"
#include "TDS/Game/TDSGameInstance.h"


ATDSCharacter::ATDSCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	InventoryComponent = CreateDefaultSubobject<UCharacterInventoryComponent>(TEXT("InventoryComponent"));

	if (InventoryComponent)
	{
		InventoryComponent->OnSwitchWeapon.AddDynamic(this, &ATDSCharacter::InitWeapon);
	}
	
	// Create a decal in the world to show the cursor's location
	//CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	//CursorToWorld->SetupAttachment(RootComponent);
	//static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("Material'/Game/Blueprints/Character/M_Cursor_Decal.M_Cursor_Decal'"));
	//if (DecalMaterialAsset.Succeeded())
	//{
	//	CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
	//}
	//CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	//CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}


void ATDSCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	/*
	if (CursorToWorld != nullptr)
	{
	if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
	{
	if (UWorld* World = GetWorld())
	{
	FHitResult HitResult;
	FCollisionQueryParams Params(NAME_None, FCollisionQueryParams::GetUnknownStatId());
	FVector StartLocation = TopDownCameraComponent->GetComponentLocation();
	FVector EndLocation = TopDownCameraComponent->GetComponentRotation().Vector() * 2000.0f;
	Params.AddIgnoredActor(this);
	World->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECC_Visibility, Params);
	FQuat SurfaceRotation = HitResult.ImpactNormal.ToOrientationRotator().Quaternion();
	CursorToWorld->SetWorldLocationAndRotation(HitResult.Location, SurfaceRotation);
	}
	}
	else if (APlayerController* PC = Cast<APlayerController>(GetController()))
	{
	FHitResult TraceHitResult;
	PC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
	FVector CursorFV = TraceHitResult.ImpactNormal;
	FRotator CursorR = CursorFV.Rotation();
	CursorToWorld->SetWorldLocation(TraceHitResult.Location);
	CursorToWorld->SetWorldRotation(CursorR);
	}
	}
	 */
    if (CurrentCursor)
    {
	    APlayerController* PC = Cast<APlayerController>(GetController());
	    if (PC)
	    {
	    	FHitResult TraceHitResult;
	    	PC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
	    	const FVector CursorForwardV = TraceHitResult.ImpactNormal;
	    	const FRotator CursorR = CursorForwardV.Rotation();
	    	CurrentCursor->SetWorldLocation(TraceHitResult.Location);
	    	CurrentCursor->SetWorldRotation(CursorR);
	    }
    }
	
	MovementTick(DeltaSeconds);
}

void ATDSCharacter::BeginPlay()
{
	Super::BeginPlay();

	if (CursorMaterial)
	{
		CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize, FVector(0));
	}	
	
}

void ATDSCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward",this,&ATDSCharacter::InputAxisX);
	PlayerInputComponent->BindAxis("MoveRight",this,&ATDSCharacter::InputAxisY);

	PlayerInputComponent->BindAction(TEXT("FireEvent"), IE_Pressed, this, &ATDSCharacter::InputAttackPressed);
	PlayerInputComponent->BindAction(TEXT("FireEvent"), IE_Released, this, &ATDSCharacter::InputAttackReleased);
	PlayerInputComponent->BindAction(TEXT("ReloadEvent"), IE_Released, this, &ATDSCharacter::InputReload);
	PlayerInputComponent->BindAction(TEXT("SwitchNextWeapon"), IE_Pressed, this, &ATDSCharacter::InputSwitchNextWeapon);
	PlayerInputComponent->BindAction(TEXT("SwitchPreviousWeapon"), IE_Pressed, this, &ATDSCharacter::InputSwitchPreviousWeapon);
}

void ATDSCharacter::InputAxisX(const float Value)
{
	AxisX = Value;
}

void ATDSCharacter::InputAxisY(const float Value)
{
	AxisY = Value;
}

void ATDSCharacter::InputAttackPressed()
{
	AttackCharacterEvent(true);
}

void ATDSCharacter::InputAttackReleased()
{
	AttackCharacterEvent(false);
}

void ATDSCharacter::MovementTick(float DeltaTime)
{
	AddMovementInput(FVector(1.0f,0.0f,0.0f),AxisX);
	AddMovementInput(FVector(0.0f,1.0f,0.0f),AxisY);
	
	if (MovementState == EMovementState::Sprint_State)
	{
		const FRotator CharacterRotator = FVector(AxisX,AxisY,0.0f).ToOrientationRotator();
		SetActorRotation(FQuat(CharacterRotator));
	} 
	else
	{
		APlayerController* MyController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
		if(MyController)
		{
			FHitResult ResultHit;
			MyController->GetHitResultUnderCursor(ECC_GameTraceChannel1,true,ResultHit);
		
			const float RotatorResultYaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(),ResultHit.Location).Yaw;
			SetActorRotation(FQuat(FRotator(0.0f,RotatorResultYaw ,0.0f)));

			if (CurrentWeapon)
			{
				FVector Displacement = FVector(0);
				switch (MovementState)
				{
				case EMovementState::Aim_State:
					Displacement = FVector(0.0f, 0.0f, 0.0f);
					CurrentWeapon->ShouldReduceDispersion = true;
					break;
				case EMovementState::AimWalk_State:
					CurrentWeapon->ShouldReduceDispersion = true;
					Displacement = FVector(0.0f, 0.0f, 0.0f);
					break;
				case EMovementState::Walk_State:
					Displacement = FVector(0.0f, 0.0f, 0.0f);
					CurrentWeapon->ShouldReduceDispersion = false;
					break;
				case EMovementState::Run_State:
					Displacement = FVector(0.0f, 0.0f, 0.0f);
					CurrentWeapon->ShouldReduceDispersion = false;
					break;
				case EMovementState::Sprint_State:
					break;
				default:
					break;
				}
				
				CurrentWeapon->ShootEndLocation = ResultHit.Location + Displacement;
				
			}
		}
	}
}

void ATDSCharacter::AttackCharacterEvent(const bool bIsFiring)
{
	
	AWeaponDefault* CharWeapon = GetCurrentWeapon();
	if (CharWeapon)
	{
		//ToDo Check melee or range
		CharWeapon->SetWeaponStateFire(bIsFiring);
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("ATDSCharacter::AttackCharacterEvent - CurrentWeapon -NULL"));
}

void ATDSCharacter::CharacterUpdate()
{
	float ResultSpeed = 300.0f;
	switch (MovementState)
	{
	case EMovementState::Aim_State: ResultSpeed = MovementInfo.AimSpeedNormal;
		break;
	case EMovementState::AimWalk_State: ResultSpeed = MovementInfo.AimSpeedWalk;
		break;
	case EMovementState::Walk_State: ResultSpeed = MovementInfo.WalkSpeedNormal;
		break;
	case EMovementState::Run_State: ResultSpeed = MovementInfo.RunSpeedNormal;
		break;
	case EMovementState::Sprint_State: ResultSpeed = MovementInfo.SprintSpeed;
		break;
	default:
		break;
	}

	GetCharacterMovement()->MaxWalkSpeed = ResultSpeed;
}

void ATDSCharacter::ChangeMovementState()
{
	if (!WalkEnabled && !SprintEnabled && !AimEnabled)
	{
		MovementState = EMovementState::Run_State;
	}
	else
	{
		if (SprintEnabled)
		{
			WalkEnabled = false;
			AimEnabled = false;
			RollEnabled = false;
			MovementState = EMovementState::Sprint_State;
		}
		if (WalkEnabled && !SprintEnabled && AimEnabled)
		{
			MovementState = EMovementState::AimWalk_State;
		}
		else
		{
			if (WalkEnabled && !SprintEnabled && !AimEnabled)
			{
				MovementState = EMovementState::Walk_State;
			}
			else
			{
				if (!WalkEnabled && !SprintEnabled && AimEnabled)
				{
					MovementState = EMovementState::Aim_State;
				}
			}
		}
	}	

	CharacterUpdate();

	//Weapon state update
	AWeaponDefault* CharacterWeapon = GetCurrentWeapon();
	
	if (CharacterWeapon)
	{
		CharacterWeapon->UpdateStateWeapon(MovementState);
	}

}

void ATDSCharacter::InputReload()
{
	if (CurrentWeapon && !CurrentWeapon->bIsReloading)
	{
		if (CurrentWeapon->GetWeaponCapacityAmmo() < CurrentWeapon->WeaponSettings.MaxCapacityAmmo && CurrentWeapon->CheckWeaponCanReload())
			CurrentWeapon->ReloadInit();
	}
}


AWeaponDefault* ATDSCharacter::GetCurrentWeapon() const
{
	return CurrentWeapon;
}


void ATDSCharacter::InitWeapon(const FName IdWeaponTitle, FAddonWeaponInfo WeaponAddonInfo,
	int32 NewCurrentIndexWeapon)
{
	if (CurrentWeapon)
	{
		CurrentWeapon->Destroy();
		CurrentWeapon = nullptr;
	}
	
	UTDSGameInstance* GameInst = Cast<UTDSGameInstance>(GetGameInstance());
	if (GameInst)
	{
		FWeaponInfo WeaponInformation;
		if (GameInst->GetWeaponInfoByName(IdWeaponTitle, WeaponInformation))
		{
			if (WeaponInformation.WeaponClass)
			{
				const FVector SpawnLocation = FVector(0);
				const FRotator SpawnRotation = FRotator(0);

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = this;
				SpawnParams.Instigator = GetInstigator();

				AWeaponDefault* Weapon = Cast<AWeaponDefault>(GetWorld()->SpawnActor(WeaponInformation.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (Weapon)
				{
					const FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
					Weapon->AttachToComponent(GetMesh(), Rule, FName("RifleSocket"));
					CurrentWeapon = Weapon;
					
					Weapon->WeaponSettings = WeaponInformation;
					//Weapon->AdditionalWeaponInfo.CapacityAmmo = WeaponInformation.MaxCapacityAmmo;
					//Weapon->ReloadTime = WeaponInfo.ReloadTime;
					Weapon->UpdateStateWeapon(MovementState);

					Weapon->AddonWeaponSettings = WeaponAddonInfo;
					//if (InventoryComponent)
						CurrentIndexWeapon = NewCurrentIndexWeapon;
					
					Weapon->OnWeaponReloadStart.AddDynamic(this, &ATDSCharacter::WeaponReloadStart);
					Weapon->OnWeaponReloadEnd.AddDynamic(this, &ATDSCharacter::WeaponReloadEnd);
					Weapon->OnWeaponFireStart.AddDynamic(this, &ATDSCharacter::WeaponFireStart);
					if (CurrentWeapon->GetWeaponCapacityAmmo() <=0 && CurrentWeapon->CheckWeaponCanReload())
						CurrentWeapon->ReloadInit();

					if(InventoryComponent)
						InventoryComponent->OnWeaponAmmoAvailable.Broadcast(Weapon->WeaponSettings.WeaponType);
				}
			}
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("ATPSCharacter::InitWeapon - Weapon not found in table -NULL"));
		}
	}
}

void ATDSCharacter::RemoveCurrentWeapon()
{
}

void ATDSCharacter::WeaponFireStart(UAnimMontage* Animation)
{
	if(InventoryComponent && CurrentWeapon)
		InventoryComponent->SetAddonInfoWeapon(CurrentIndexWeapon, CurrentWeapon->AddonWeaponSettings);
	WeaponFireStart_BP(Animation);
}

void ATDSCharacter::WeaponFireStart_BP_Implementation(UAnimMontage* Animation)
{
}

void ATDSCharacter::WeaponReloadStart(UAnimMontage* Animation)
{
	WeaponReloadStart_BP(Animation);
}

void ATDSCharacter::WeaponReloadEnd(bool bIsSuccess, int32 AmmoTake)
{
	if (InventoryComponent && CurrentWeapon)
	{
		InventoryComponent->AmmoSlotChangeValue(CurrentWeapon->WeaponSettings.WeaponType, AmmoTake);
		InventoryComponent->SetAddonInfoWeapon(CurrentIndexWeapon, CurrentWeapon->AddonWeaponSettings);
	}
	WeaponReloadEnd_BP(bIsSuccess);
}

void ATDSCharacter::WeaponReloadStart_BP_Implementation(UAnimMontage* Animation)
{
}

void ATDSCharacter::WeaponReloadEnd_BP_Implementation(bool bIsSuccess)
{
}

UDecalComponent* ATDSCharacter::GetCursorToWorld() const
{
	return CurrentCursor;
}

void ATDSCharacter::InputSwitchNextWeapon()
{
	if (InventoryComponent->WeaponSlots.Num() > 1)
	{
		//We have more then one weapon go switch
		const int8 OldIndex = CurrentIndexWeapon;
		FAddonWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->AddonWeaponSettings;
			if(CurrentWeapon->bIsReloading)
				CurrentWeapon->ReloadCancel();
		}
			
		if (InventoryComponent)
		{			
			if (InventoryComponent->SwitchWeaponToIndex(CurrentIndexWeapon + 1, OldIndex, OldInfo,true))
			{ }
		}
	}	
}

void ATDSCharacter::InputSwitchPreviousWeapon()
{
	if (InventoryComponent->WeaponSlots.Num() > 1)
	{
		//We have more then one weapon go switch
		const int8 OldIndex = CurrentIndexWeapon;
		FAddonWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->AddonWeaponSettings;
			if (CurrentWeapon->bIsReloading)
				CurrentWeapon->ReloadCancel();
		}

		if (InventoryComponent)
		{
			//InventoryComponent->SetAdditionalInfoWeapon(OldIndex, GetCurrentWeapon()->AdditionalWeaponInfo);
			if (InventoryComponent->SwitchWeaponToIndex(CurrentIndexWeapon - 1,OldIndex, OldInfo, false))
			{ }
		}
	}
}


