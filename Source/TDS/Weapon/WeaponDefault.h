// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/ArrowComponent.h"
#include "TDS/FuncLibrary/Types.h"
#include "TDS/Weapon/ProjectileDefault.h"
#include "WeaponDefault.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponFireStart,UAnimMontage*,AnimationFire);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponReloadStart,UAnimMontage*,AnimationReload);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponReloadEnd, bool, bIsSuccess, int32, LeftAmmo);


UCLASS()
class TDS_API AWeaponDefault : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWeaponDefault();

	FOnWeaponFireStart OnWeaponFireStart;
	FOnWeaponReloadStart OnWeaponReloadStart;
	FOnWeaponReloadEnd OnWeaponReloadEnd;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class USceneComponent* SceneComponent = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class USkeletalMeshComponent* SkeletalMeshWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UStaticMeshComponent* StaticMeshWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UArrowComponent* ShootLocation = nullptr;

	UPROPERTY()
	FWeaponInfo WeaponSettings;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Info")
	FAddonWeaponInfo AddonWeaponSettings;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:

	//Timers
	float FireTimer = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ReloadLogic")
	float ReloadTimer = 0.0f;
	

	float DropMagazineTimer = -1.0f;
	float DropShellTimer = -1.0f;
	
	//Flags
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
	bool bIsFiring = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
	bool bIsBlockFire = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ReloadLogic")
	bool bIsReloading = false;
	bool bIsAiming = false;
	
	bool bIsDropMagazine = false;
	bool bIsDropShell = false;
	
	//Dispersion
	bool ShouldReduceDispersion = false;
	float CurrentDispersion = 0.0f;
	float CurrentDispersionMax = 1.0f;
	float CurrentDispersionMin = 0.1f;
	float CurrentDispersionRecoil = 0.1f;
	float CurrentDispersionReduction = 0.1f;

	FVector ShootEndLocation = FVector(0);
	
	//Weapon Transform
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponTransform")
	FTransform WeaponFire;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponTransform")
	FTransform WeaponLower;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponTransform")
	FTransform WeaponAim;
	
	// Tick Func
	virtual void Tick(float DeltaTime) override;

	void FireTick(float DeltaTime);
	void ReloadTick(float DeltaTime);
	void DispersionTick(float DeltaTime);
	void MagazineDropTick(float DeltaTime);
	void ShellDropTick(float DeltaTime);

	//Func
	UFUNCTION()
	void Fire();
	UFUNCTION()
	void WeaponInit();
	UFUNCTION()
	void ReloadInit();
	UFUNCTION()
	void ReloadFinish();
	UFUNCTION()
	void ReloadCancel();
	UFUNCTION()
	void DropMeshInit(UStaticMesh* DropMesh, FTransform Offset, FVector DropImpulseDirection, float LifeTimeMesh,
	                  float ImpulseRandomDispersion, float PowerImpulse, float CustomMass);

	UFUNCTION(BlueprintCallable)
	int32 GetWeaponCapacityAmmo() const;
	int8 GetAvailableAmmoForReload() const;
	bool CheckWeaponCanFire() const;
	bool CheckWeaponCanReload() const;
	FVector GetFireEndLocation()const;
	float GetCurrentDispersion() const;
	FProjectileInfo GetProjectile() const;
	int8 GetProjectileByShot() const;
	
	
	UFUNCTION(BlueprintCallable)
	void SetWeaponStateFire(const bool bIsFire);
	void UpdateStateWeapon(EMovementState NewMovementState);
	void ChangeDispersionByShot();
	FVector ApplyDispersionToShoot(FVector DirectionShoot)const;
	

	//Debug
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	bool ShowDebug = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	float SizeVectorToChangeShootDirectionLogic = 100.0f;


};
