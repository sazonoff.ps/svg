// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS/Weapon/ProjectileDefault_Grenade.h"

#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"

int32 DebugExplodeShow = 0;
FAutoConsoleVariableRef CVARExplodeShow(
	TEXT("TDS.DebugExplode"),
	DebugExplodeShow,
	TEXT("Draw Debug for Explode"),
	ECVF_Cheat);


void AProjectileDefault_Grenade::BeginPlay()
{
	Super::BeginPlay();
	
}

void AProjectileDefault_Grenade::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	TimerExplosion(DeltaTime);


}

void AProjectileDefault_Grenade::TimerExplosion(float DeltaTime)
{
	if (TimerEnabled)
	{
		if (TimerToExplosion > TimeToExplosion)
		{
			Explosion();
		}
		else
		{
			TimerToExplosion += DeltaTime;
		}
	}
}

void AProjectileDefault_Grenade::BulletCollisionSphereHit(class UPrimitiveComponent* HitComp, AActor* OtherActor,
                                                          UPrimitiveComponent* OtherComp, FVector NormalImpulse,
                                                          const FHitResult& Hit)
{
	Super::BulletCollisionSphereHit(HitComp, OtherActor, OtherComp, NormalImpulse, Hit);
}

void AProjectileDefault_Grenade::ProjectileImpact()
{
	//Init Grenade
	TimerEnabled = true;
}

void AProjectileDefault_Grenade::Explosion()
{

	if (DebugExplodeShow)
	{
		DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSettings.ProjectileMinRadiusDamage, 12, FColor::Green,
		                false, 12.0f);
		DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSettings.ProjectileMaxRadiusDamage, 12, FColor::Red,
		                false, 12.0f);
	}
	
	TimerEnabled = false;
	if (ProjectileSettings.ExplosionFX)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ProjectileSettings.ExplosionFX, GetActorLocation(), GetActorRotation(), FVector(1.0f));
	}
	if (ProjectileSettings.ExplosionSound)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), ProjectileSettings.ExplosionSound, GetActorLocation());
	}
	
	TArray<AActor*> const IgnoredActor;
	UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(),
		ProjectileSettings.ExplosionMaxDamage,
		ProjectileSettings.ExplosionMaxDamage*0.2f,
		GetActorLocation(),
		ProjectileSettings.ProjectileMinRadiusDamage,
		ProjectileSettings.ProjectileMaxRadiusDamage,
		5,
		NULL, IgnoredActor,nullptr,nullptr);

	this->Destroy();
}