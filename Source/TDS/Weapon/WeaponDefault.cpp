// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS/Weapon/WeaponDefault.h"

#include "DrawDebugHelpers.h"
#include "Engine/StaticMeshActor.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "TDS/Components/CharacterInventoryComponent.h"

// Sets default values
AWeaponDefault::AWeaponDefault()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;

	SkeletalMeshWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Mesh"));
	SkeletalMeshWeapon->SetGenerateOverlapEvents(false);
	SkeletalMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	SkeletalMeshWeapon->SetupAttachment(RootComponent);

	StaticMeshWeapon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	StaticMeshWeapon->SetGenerateOverlapEvents(false);
	StaticMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	StaticMeshWeapon->SetupAttachment(RootComponent);

	ShootLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("ShootLocation"));
	ShootLocation->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AWeaponDefault::BeginPlay()
{
	Super::BeginPlay();

	WeaponInit();
}

// Called every frame
void AWeaponDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FireTick(DeltaTime);
	ReloadTick(DeltaTime);
	DispersionTick(DeltaTime);
	MagazineDropTick(DeltaTime);
	ShellDropTick(DeltaTime);
}

void AWeaponDefault::FireTick(const float DeltaTime)
{
	if (bIsFiring && GetWeaponCapacityAmmo() > 0 && !bIsReloading)
	{
		if (FireTimer < 0.f)
		{
			Fire();
		}
		else
			FireTimer -= DeltaTime;
	}
}

void AWeaponDefault::ReloadTick(const float DeltaTime)
{
	if (bIsReloading)
	{
		if (ReloadTimer < 0.0f)
		{
			ReloadFinish();
		}
		else
		{
			ReloadTimer -= DeltaTime;
		}
	}
}

void AWeaponDefault::DispersionTick(float DeltaTime)
{
	if (!bIsReloading)
	{
		if (!bIsFiring)
		{
			if (ShouldReduceDispersion)
				CurrentDispersion = CurrentDispersion - CurrentDispersionReduction;
			else
				CurrentDispersion = CurrentDispersion + CurrentDispersionReduction;
		}				

		if (CurrentDispersion < CurrentDispersionMin)
		{
			CurrentDispersion = CurrentDispersionMin;
		}
		else
		{
			if (CurrentDispersion > CurrentDispersionMax)
			{
				CurrentDispersion = CurrentDispersionMax;
			}
		}
	}
	if(ShowDebug)
		UE_LOG(LogTemp, Warning, TEXT("Dispersion: MAX = %f. MIN = %f. Current = %f"), CurrentDispersionMax, CurrentDispersionMin, CurrentDispersion);

}

void AWeaponDefault::MagazineDropTick(const float DeltaTime)
{
	if (bIsDropMagazine)
	{
		if (DropMagazineTimer < 0.0f)
		{
			bIsDropMagazine = false;
			DropMeshInit(WeaponSettings.MagazineDropMesh.DropMesh, WeaponSettings.MagazineDropMesh.DropMeshOffset,
						WeaponSettings.MagazineDropMesh.DropMeshImpulseDirection,
						WeaponSettings.MagazineDropMesh.DropMeshLifeTime,
						WeaponSettings.MagazineDropMesh.ImpulseRandomDispersion, WeaponSettings.MagazineDropMesh.PowerImpulse,
						WeaponSettings.MagazineDropMesh.CustomMass);
		}
		else
			DropMagazineTimer -= DeltaTime;
	}
}

void AWeaponDefault::ShellDropTick(float DeltaTime)
{
	if (bIsDropShell)
	{
		if (DropShellTimer < 0.0f)
		{
			bIsDropShell = false;
			DropMeshInit(WeaponSettings.ShellBullets.DropMesh, WeaponSettings.ShellBullets.DropMeshOffset,
						WeaponSettings.ShellBullets.DropMeshImpulseDirection,
						WeaponSettings.ShellBullets.DropMeshLifeTime,
						WeaponSettings.ShellBullets.ImpulseRandomDispersion, WeaponSettings.ShellBullets.PowerImpulse,
						WeaponSettings.ShellBullets.CustomMass);
		}
		else
			DropMagazineTimer -= DeltaTime;
	}
}

void AWeaponDefault::Fire()
{

	UAnimMontage* AnimationToPlay = nullptr;
	if (bIsAiming)
		AnimationToPlay = WeaponSettings.AnimationSettings.CharacterFireAnimationAim;
	else
		AnimationToPlay = WeaponSettings.AnimationSettings.CharacterFireAnimation;

	if (WeaponSettings.AnimationSettings.WeaponFireAnimation && SkeletalMeshWeapon && SkeletalMeshWeapon->GetAnimInstance())
	{
		SkeletalMeshWeapon->GetAnimInstance()->Montage_Play(WeaponSettings.AnimationSettings.WeaponFireAnimation);
	}
	
	if (WeaponSettings.ShellBullets.DropMesh)
	{
		if (WeaponSettings.ShellBullets.DropMeshTime < 0.0f)
		{
			DropMeshInit(WeaponSettings.ShellBullets.DropMesh, WeaponSettings.ShellBullets.DropMeshOffset,
			             WeaponSettings.ShellBullets.DropMeshImpulseDirection,
			             WeaponSettings.ShellBullets.DropMeshLifeTime,
			             WeaponSettings.ShellBullets.ImpulseRandomDispersion, WeaponSettings.ShellBullets.PowerImpulse,
			             WeaponSettings.ShellBullets.CustomMass);
		}
		else
		{
			bIsDropShell = true;
			DropShellTimer = WeaponSettings.ShellBullets.DropMeshTime;
		}
	}
	
	FireTimer = WeaponSettings.RateOfFire;
	//AddonWeaponSettings.CapacityAmmo --;
	AddonWeaponSettings.CapacityAmmo = AddonWeaponSettings.CapacityAmmo - 1;
	ChangeDispersionByShot();
	
	OnWeaponFireStart.Broadcast(AnimationToPlay);

	UGameplayStatics::SpawnSoundAtLocation(GetWorld(), WeaponSettings.SoundFireWeapon, ShootLocation->GetComponentLocation());
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), WeaponSettings.EffectFireWeapon, ShootLocation->GetComponentTransform());

	const int8 NumberProjectile = GetProjectileByShot();

	if (ShootLocation)
	{
		const FVector SpawnLocation = ShootLocation->GetComponentLocation();
		FRotator SpawnRotation = ShootLocation->GetComponentRotation();
		const FProjectileInfo ProjectileInfo = GetProjectile();
		
		for (int8 i = 0; i < NumberProjectile; i++)//Shotgun
			{
			FVector EndLocation = GetFireEndLocation(); 

			if (ProjectileInfo.Projectile)
			{
				//Projectile Init ballistic fire
				FVector Direction = EndLocation - SpawnLocation;

				Direction.Normalize();

				FMatrix TempMatrix(Direction, FVector(0, 1, 0), FVector(0, 0, 1), FVector::ZeroVector);
				SpawnRotation = TempMatrix.Rotator();
				
				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = GetOwner();
				SpawnParams.Instigator = GetInstigator();

				if (AProjectileDefault* TempProjectile = Cast<AProjectileDefault>(
					GetWorld()->SpawnActor(ProjectileInfo.Projectile, &SpawnLocation, &SpawnRotation, SpawnParams)))
				{													
					TempProjectile->ProjectileInit(WeaponSettings.ProjectileSettings);				
				}
			}
			else
			{
				FHitResult Hit;
				TArray<AActor*> Actors;				

				EDrawDebugTrace::Type DebugTrace;
				if (ShowDebug)
					{
						DrawDebugLine(GetWorld(), SpawnLocation,
								SpawnLocation + ShootLocation->GetForwardVector() * WeaponSettings.TraceDistance,
										FColor::Black, false, 5.f, (uint8)'\000', 0.5f);
										DebugTrace = EDrawDebugTrace::ForDuration;
					}
				else
					DebugTrace = EDrawDebugTrace::None;
				
				UKismetSystemLibrary::LineTraceSingle(GetWorld(), SpawnLocation, EndLocation * WeaponSettings.TraceDistance,
					TraceTypeQuery4, false, Actors, DebugTrace, Hit,
						true, FLinearColor::Red,FLinearColor::Green, 5.0f);
		
				if (Hit.GetActor() && Hit.PhysMaterial.IsValid())
				{
					EPhysicalSurface CurrentSurfaceType = UGameplayStatics::GetSurfaceType(Hit);

					if (WeaponSettings.ProjectileSettings.HitDecals.Contains(CurrentSurfaceType))
					{
						UMaterialInterface* CurrentMaterial = WeaponSettings.ProjectileSettings.HitDecals[CurrentSurfaceType];

						if (CurrentMaterial && Hit.GetComponent())
						{
							UGameplayStatics::SpawnDecalAttached(CurrentMaterial, FVector(20.0f), Hit.GetComponent(),
							                                     NAME_None, Hit.ImpactPoint,
							                                     Hit.ImpactNormal.Rotation(),
							                                     EAttachLocation::KeepWorldPosition, 10.0f);
						}
					}
					if (WeaponSettings.ProjectileSettings.HitFXs.Contains(CurrentSurfaceType))
					{
						UParticleSystem* CurrentParticle = WeaponSettings.ProjectileSettings.HitFXs[CurrentSurfaceType];
						if (CurrentParticle)
						{
							UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), CurrentParticle,
							                                         FTransform(Hit.ImpactNormal.Rotation(),
							                                         Hit.ImpactPoint, FVector(1.0f)));
						}
					}

					if (WeaponSettings.ProjectileSettings.HitSound)
					{
						UGameplayStatics::PlaySoundAtLocation(GetWorld(), WeaponSettings.ProjectileSettings.HitSound, Hit.ImpactPoint);
					}
					UGameplayStatics::ApplyPointDamage(Hit.GetActor(),
					                                   WeaponSettings.ProjectileSettings.ProjectileDamage,
					                                   Hit.TraceStart, Hit, GetInstigatorController(), this,nullptr);
				}
			}
		}				
	}
	
	if (GetWeaponCapacityAmmo() <= 0 && !bIsReloading)
	{
		if(CheckWeaponCanReload())
			ReloadInit();
	}
}

void AWeaponDefault::WeaponInit()
{
	if (SkeletalMeshWeapon && !SkeletalMeshWeapon->SkeletalMesh)
	{
		SkeletalMeshWeapon->DestroyComponent(true);
	}

	if (StaticMeshWeapon && !StaticMeshWeapon->GetStaticMesh())
	{
		StaticMeshWeapon->DestroyComponent();
	}

	UpdateStateWeapon(EMovementState::Run_State);
}

void AWeaponDefault::ReloadInit()
{
	bIsReloading = true;

	ReloadTimer = WeaponSettings.ReloadTime;

	UAnimMontage* AnimationToPlay = nullptr;
	
	if (bIsAiming)
		AnimationToPlay = WeaponSettings.AnimationSettings.CharacterReloadAnimationAim;
	else
		AnimationToPlay = WeaponSettings.AnimationSettings.CharacterReloadAnimation;

	OnWeaponReloadStart.Broadcast(AnimationToPlay);

	UAnimMontage* AnimationWeaponToPlay = nullptr;
	
	if (bIsAiming)
		AnimationWeaponToPlay = WeaponSettings.AnimationSettings.WeaponReloadAnimation;
	else
		AnimationWeaponToPlay = WeaponSettings.AnimationSettings.WeaponReloadAnimation;

	if (WeaponSettings.AnimationSettings.WeaponReloadAnimation && SkeletalMeshWeapon && SkeletalMeshWeapon->GetAnimInstance())
		SkeletalMeshWeapon->GetAnimInstance()->Montage_Play(AnimationWeaponToPlay);

	if (WeaponSettings.MagazineDropMesh.DropMesh)
	{
		bIsDropMagazine = true;
		DropMagazineTimer = WeaponSettings.MagazineDropMesh.DropMeshTime;
	}
}

void AWeaponDefault::ReloadFinish()
{
	bIsReloading = false;

	const int8 AvailableAmmoInInventory = GetAvailableAmmoForReload();
	int8 AmmoNeedTakeFromInv;
	const int8 AmmoToReload = WeaponSettings.MaxCapacityAmmo - AddonWeaponSettings.CapacityAmmo;

	if (AmmoToReload > AvailableAmmoInInventory)
	{
		AddonWeaponSettings.CapacityAmmo = AvailableAmmoInInventory;
		AmmoNeedTakeFromInv = AvailableAmmoInInventory;
	}
	else
	{
		AddonWeaponSettings.CapacityAmmo += AmmoToReload;
		AmmoNeedTakeFromInv = AmmoToReload;
	}
		
	OnWeaponReloadEnd.Broadcast(true, -AmmoNeedTakeFromInv);
	
}

void AWeaponDefault::ReloadCancel()
{
	bIsReloading = false;
	if(SkeletalMeshWeapon && SkeletalMeshWeapon->GetAnimInstance())
		SkeletalMeshWeapon->GetAnimInstance()->StopAllMontages(0.15f);

	OnWeaponReloadEnd.Broadcast(false, 0);
	bIsDropMagazine = false;
}

void AWeaponDefault::DropMeshInit(UStaticMesh* DropMesh, FTransform Offset, FVector DropImpulseDirection,
                                  float LifeTimeMesh, float ImpulseRandomDispersion, float PowerImpulse, float CustomMass)
{
	if (IsValid(DropMesh))
	{
		FTransform Transform;
		
		FVector LocalDir = this->GetActorForwardVector() * Offset.GetLocation().X + this->GetActorRightVector() *
			Offset.GetLocation().Y + this->GetActorUpVector() * Offset.GetLocation().Z;
		
		Transform.SetLocation(GetActorLocation() + LocalDir);
		Transform.SetScale3D(Offset.GetScale3D());
		Transform.SetRotation((GetActorRotation() + Offset.Rotator()).Quaternion());
		AStaticMeshActor* NewActor = nullptr;

		FActorSpawnParameters Params;
		Params.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
		Params.Owner = this;
		NewActor = GetWorld()->SpawnActor<AStaticMeshActor>(AStaticMeshActor::StaticClass(), Transform, Params);

		if (NewActor && NewActor->GetStaticMeshComponent())
		{
			NewActor->GetStaticMeshComponent()->SetCollisionProfileName(TEXT("IgnoreOnlyPawn"));
			NewActor->GetStaticMeshComponent()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
			NewActor->SetActorTickEnabled(false);
			NewActor->InitialLifeSpan = LifeTimeMesh;
			NewActor->SetLifeSpan(LifeTimeMesh);
			NewActor->GetStaticMeshComponent()->Mobility = EComponentMobility::Movable;
			NewActor->GetStaticMeshComponent()->SetSimulatePhysics(true);
			NewActor->GetStaticMeshComponent()->SetStaticMesh(DropMesh);
			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_GameTraceChannel1, ECollisionResponse::ECR_Ignore);
			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_GameTraceChannel2, ECollisionResponse::ECR_Ignore);
			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse::ECR_Ignore);
			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_WorldStatic, ECollisionResponse::ECR_Block);
			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_WorldDynamic, ECollisionResponse::ECR_Block);
			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_PhysicsBody, ECollisionResponse::ECR_Block);

			if (CustomMass > 0.0f)
			{
				NewActor->GetStaticMeshComponent()->SetMassOverrideInKg(NAME_None, CustomMass, true);
			}

			if (!DropImpulseDirection.IsNearlyZero())
			{
				FVector FinalDir = FVector(0);
				
				LocalDir += (DropImpulseDirection * 1000.0f);
				
				if (!FMath::IsNearlyZero(ImpulseRandomDispersion))
				{
					FinalDir += UKismetMathLibrary::RandomUnitVectorInConeInDegrees(LocalDir, ImpulseRandomDispersion);
				}

				NewActor->GetStaticMeshComponent()->AddImpulse(FinalDir.GetSafeNormal(0.0001f) * PowerImpulse);
			}
		}
	}
}

int32 AWeaponDefault::GetWeaponCapacityAmmo() const
{
	return AddonWeaponSettings.CapacityAmmo;
}

bool AWeaponDefault::CheckWeaponCanFire() const
{
	return  !bIsBlockFire;
}

bool AWeaponDefault::CheckWeaponCanReload() const
{
	bool Result = true;
	if (GetOwner())
	{
		UCharacterInventoryComponent* LocalInventory = Cast<UCharacterInventoryComponent>(GetOwner()->GetComponentByClass(UCharacterInventoryComponent::StaticClass()));
		if (LocalInventory)
		{
			int8 AvailableAmmoForWeapon;
			if (!LocalInventory->CheckAmmoForWeapon(WeaponSettings.WeaponType, AvailableAmmoForWeapon))
			{				
				Result = false;
			}
		}
	}

	return Result;
}

FVector AWeaponDefault::GetFireEndLocation() const
{
	bool bShootDirection = false;
	FVector EndLocation;
	
	const FVector TempVector = (ShootLocation->GetComponentLocation() - ShootEndLocation);

	if(TempVector.Size() > SizeVectorToChangeShootDirectionLogic)
	{
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot(
			(ShootLocation->GetComponentLocation() - ShootEndLocation).GetSafeNormal()) * -20000.0f;
		if(ShowDebug)
			DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(),
			              -(ShootLocation->GetComponentLocation() - ShootEndLocation), WeaponSettings.TraceDistance,
			              GetCurrentDispersion() * PI / 180.f, GetCurrentDispersion() * PI / 180.f, 32, FColor::Emerald,
			              false, .1f, (uint8)'\000', 1.0f);
	}	
	else
	{
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot(ShootLocation->GetForwardVector()) * 20000.0f;
		if (ShowDebug)
			DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetForwardVector(),
			              WeaponSettings.TraceDistance, GetCurrentDispersion() * PI / 180.f,
			              GetCurrentDispersion() * PI / 180.f, 32, FColor::Emerald, false, .1f, (uint8)'\000', 1.0f);
	}
		

	if (ShowDebug)
	{
		//direction weapon look
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetComponentLocation() + ShootLocation->GetForwardVector() * 500.0f, FColor::Cyan, false, 5.f, (uint8)'\000', 0.5f);
		//direction projectile must fly
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), ShootEndLocation, FColor::Red, false, 5.f, (uint8)'\000', 0.5f);
		//Direction Projectile Current fly
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), EndLocation, FColor::Black, false, 5.f, (uint8)'\000', 0.5f);
		
	}
		

	return EndLocation;
}

float AWeaponDefault::GetCurrentDispersion() const
{
	const float Result = CurrentDispersion;
	return Result;
}

FProjectileInfo AWeaponDefault::GetProjectile() const
{
	return WeaponSettings.ProjectileSettings;
}

int8 AWeaponDefault::GetProjectileByShot() const
{
	return WeaponSettings.ProjectileByShot;
}

void AWeaponDefault::SetWeaponStateFire(const bool bIsFire)
{
	if (CheckWeaponCanFire())
		bIsFiring = bIsFire;
	else
		bIsFiring = false;
		FireTimer = 0.01f;//!!!!!
}

void AWeaponDefault::UpdateStateWeapon(const EMovementState NewMovementState)
{
	
	bIsBlockFire = false;

	switch (NewMovementState)
	{
	case EMovementState::Aim_State:
		
		bIsAiming = true;
		CurrentDispersionMax = WeaponSettings.DispersionWeapon.Aim_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSettings.DispersionWeapon.Aim_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSettings.DispersionWeapon.Aim_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSettings.DispersionWeapon.Aim_StateDispersionReduction;
		break;
	case EMovementState::AimWalk_State:

		bIsAiming = true;
		CurrentDispersionMax = WeaponSettings.DispersionWeapon.AimWalk_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSettings.DispersionWeapon.AimWalk_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSettings.DispersionWeapon.AimWalk_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSettings.DispersionWeapon.Aim_StateDispersionReduction;
		break;
	case EMovementState::Walk_State:

		bIsAiming = false;
		CurrentDispersionMax = WeaponSettings.DispersionWeapon.Walk_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSettings.DispersionWeapon.Walk_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSettings.DispersionWeapon.Walk_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSettings.DispersionWeapon.Aim_StateDispersionReduction;
		break;
	case EMovementState::Run_State:

		bIsAiming = false;
		CurrentDispersionMax = WeaponSettings.DispersionWeapon.Run_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSettings.DispersionWeapon.Run_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSettings.DispersionWeapon.Run_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSettings.DispersionWeapon.Aim_StateDispersionReduction;
		break;
	case EMovementState::Sprint_State:

		bIsAiming = false;
		bIsBlockFire = true;
		SetWeaponStateFire(false);//set fire trigger to false
		//Block Fire
		break;
	default:
		break;
	}
}

void AWeaponDefault::ChangeDispersionByShot()
{
	CurrentDispersion += CurrentDispersionRecoil;
}

FVector AWeaponDefault::ApplyDispersionToShoot(const FVector DirectionShoot) const
{
	return FMath::VRandCone(DirectionShoot, GetCurrentDispersion() * PI / 180.f);
}


int8 AWeaponDefault::GetAvailableAmmoForReload() const
{
	int8 AvailableAmmoForWeapon = WeaponSettings.MaxCapacityAmmo;
	if (GetOwner())
	{
		UCharacterInventoryComponent* LocalInventory = Cast<UCharacterInventoryComponent>(GetOwner()->GetComponentByClass(UCharacterInventoryComponent::StaticClass()));
		if (LocalInventory)
		{			
			if (LocalInventory->CheckAmmoForWeapon(WeaponSettings.WeaponType, AvailableAmmoForWeapon))
			{
				AvailableAmmoForWeapon = AvailableAmmoForWeapon;
			}
		}
	}
	return AvailableAmmoForWeapon;
}

