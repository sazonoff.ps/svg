﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "CoreMinimal.h"
#include "TDS/Components/CharacterAnimationComponent.h"
#include "Kismet/BlueprintFunctionLibrary.h"

#include "FuncLib.generated.h"

UCLASS()
class TDS_API UFuncLib : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:

	FORCEINLINE static UCharacterAnimationComponent* GetAnimationComponent(AActor* Actor)
	{
		return IsValid(Actor)
			? Cast<UCharacterAnimationComponent>(Actor->GetComponentByClass(UCharacterAnimationComponent::StaticClass()))
			: nullptr;
	}
	

private:

	UFUNCTION(BlueprintCallable, BlueprintPure, DisplayName = "Get Animation Component", Category="Anim")
	static UCharacterAnimationComponent* K2_GetAnimationComponent(AActor* Actor)
	{
		return GetAnimationComponent(Actor);
	};
	
};
