// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS/Components/CharacterAnimationComponent.h"

#include "Kismet/GameplayStatics.h"
#include "GameFramework/Character.h"
#include "Kismet/KismetMathLibrary.h"
#include "Net/UnrealNetwork.h"

// Sets default values for this component's properties
UCharacterAnimationComponent::UCharacterAnimationComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	RotationMethod = ERotationMethod::RotateToVelocity;
    AimOffsetType = EAimOffsets::Look;
    AimOffsetBehavior = EAimOffsetClamp::Left;
    CameraBased = true;
    
    SetIsReplicatedByDefault(true);
}

void UCharacterAnimationComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UCharacterAnimationComponent, BasePose);
	DOREPLIFETIME(UCharacterAnimationComponent, OverlayPose);
	DOREPLIFETIME(UCharacterAnimationComponent, RotationMethod);
	DOREPLIFETIME(UCharacterAnimationComponent, RotationSpeed);
	DOREPLIFETIME(UCharacterAnimationComponent, TurnStartAngle);
	DOREPLIFETIME(UCharacterAnimationComponent, TurnStopTolerance);
	DOREPLIFETIME(UCharacterAnimationComponent, AimOffsetType);
	DOREPLIFETIME(UCharacterAnimationComponent, AimOffsetBehavior);
	DOREPLIFETIME(UCharacterAnimationComponent, AnimModTags);
}

// Called when the game starts
void UCharacterAnimationComponent::BeginPlay()
{
	Super::BeginPlay();

	RecastOwner();
    SetupBasePose(BasePose);
    SetupOverlayPose(OverlayPose);
    SetupRotation(RotationMethod, RotationSpeed, TurnStartAngle, TurnStopTolerance);
    SetupAimOffset(AimOffsetType, AimOffsetBehavior, AimClamp, CameraBased, AimSocketName, LookAtSocketName);

	
}


// Called every frame
void UCharacterAnimationComponent::TickComponent(const float DeltaTime, const ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if(!IsValid(OwningCharacter))
    	{
    		return;
    	}
    
    	AimTick();
    	TurnInPlaceTick();
}

void UCharacterAnimationComponent::SetupBasePose(FGameplayTag InBasePose)
{
	const FGameplayTag OldValue = OverlayPose;
	BasePose = InBasePose;
	OnBasePoseChanged.Broadcast(OldValue, OverlayPose);
}

void UCharacterAnimationComponent::SetupOverlayPose(FGameplayTag InOverlayPose)
{
	const FGameplayTag OldValue = OverlayPose;
	OverlayPose = InOverlayPose;
	OnOverlayPoseChanged.Broadcast(OldValue, OverlayPose);
	
}



void UCharacterAnimationComponent::SetupRotation(
	const ERotationMethod InRotationMethod,
	const float InRotationSpeed,
	const float InTurnStartAngle,
	const float InTurnStopTolerance)
{
	RotationMethod = InRotationMethod;
	HandleRotationMethodChange();
	RotationSpeed = InRotationSpeed;
	HandleRotationSpeedChange();
	TurnStartAngle = InTurnStartAngle;
	TurnStopTolerance = InTurnStopTolerance;
	
}

void UCharacterAnimationComponent::SetupAimOffset(
        const EAimOffsets InAimOffsetType,
        const EAimOffsetClamp InAimBehavior,
        const float InAimClamp,
        const bool InCameraBased,
        const FName InAimSocketName,
        const FName InLookAtSocketName)
{
	AimOffsetType = InAimOffsetType;
	AimOffsetBehavior = InAimBehavior;
	AimClamp = InAimClamp;
	CameraBased = InCameraBased;
	AimSocketName = InAimSocketName;
	LookAtSocketName = InLookAtSocketName;
	
}

void UCharacterAnimationComponent::AddTag(FGameplayTag InTag)
{
	AnimModTags.AddTag(InTag);
}

bool UCharacterAnimationComponent::RemoveTag(FGameplayTag InTag)
{
	return AnimModTags.RemoveTag(InTag);
}

void UCharacterAnimationComponent::AimTick()
{
	if(OwningCharacter && OwningCharacter->IsLocallyControlled())
	{
		if(OwningCharacter->IsPlayerControlled() && CameraBased)
		{
			LookAtIfPlayerControlled();
		}
		else
		{
			AimOffset = OwningCharacter->GetControlRotation();
			LookAtWithoutCamera();
		}
	}
}

void UCharacterAnimationComponent::TurnInPlaceTick()
{
	if(RotationMethod == ERotationMethod::DesiredAtAngle)
	{
		const float Delta = UKismetMathLibrary::NormalizedDeltaRotator(AimOffset, GetOwner()->GetActorRotation()).Yaw;
		const float AbsoluteDelta = UKismetMathLibrary::Abs(Delta);
		const float Speed = GetOwner()->GetVelocity().Size();
		if(AbsoluteDelta > TurnStartAngle || Speed > 25.0f)
		{
			OwnerMovementComponent->bUseControllerDesiredRotation = true;
		}
		else
		{
			const float ClampValue = FMath::Clamp(TurnStopTolerance, 1.0f, 90.0f);
			const float Min = ClampValue/-1.0f;
			const float Max = ClampValue/1.0f;
			const bool bInRange = UKismetMathLibrary::InRange_FloatFloat(Delta, Min, Max, true, true);
			if(bInRange)
			{
				OwnerMovementComponent->bUseControllerDesiredRotation = false;
			}
		}
	}
}

void UCharacterAnimationComponent::RecastOwner()
{
	// If the owner is invalid, return.
	if(!GetOwner())
	{
		return;
	}
	OwningCharacter = Cast<ACharacter>(GetOwner());


	// If the owning character is invalid, return.
	if(!OwningCharacter)
	{
		return;
	}

	OwnerMovementComponent = OwningCharacter->GetCharacterMovement();
}

void UCharacterAnimationComponent::HandleRotationSpeedChange()
{
	if(!OwnerMovementComponent)
	{
		RecastOwner();
	}
	OwnerMovementComponent->RotationRate.Yaw = RotationSpeed;
}

void UCharacterAnimationComponent::HandleRotationMethodChange()
{
	if(!OwningCharacter)
	{
		RecastOwner();
	}

	switch(RotationMethod)
	{
		case ERotationMethod::None:
			OwningCharacter->bUseControllerRotationYaw = false;
			OwnerMovementComponent->bOrientRotationToMovement = false;
			OwnerMovementComponent->bUseControllerDesiredRotation = false;
			break;
		case ERotationMethod::RotateToVelocity:
			OwningCharacter->bUseControllerRotationYaw = false;
			OwnerMovementComponent->bOrientRotationToMovement = true;
			OwnerMovementComponent->bUseControllerDesiredRotation = false;
			break;
		case ERotationMethod::DesiredAtAngle:
			OwningCharacter->bUseControllerRotationYaw = false;
			OwnerMovementComponent->bOrientRotationToMovement = false;
			OwnerMovementComponent->bUseControllerDesiredRotation = false;
			break;
		case ERotationMethod::DesiredRotation:
			OwningCharacter->bUseControllerRotationYaw = false;
			OwnerMovementComponent->bOrientRotationToMovement = false;
			OwnerMovementComponent->bUseControllerDesiredRotation = true;
			break;
		case ERotationMethod::AbsoluteRotation:
			OwningCharacter->bUseControllerRotationYaw = true;
			OwnerMovementComponent->bOrientRotationToMovement = false;
			OwnerMovementComponent->bUseControllerDesiredRotation = false;
			break;

		default:
			checkNoEntry();
	}
}

void UCharacterAnimationComponent::LookAtIfPlayerControlled()
{
	APlayerCameraManager* PlayerCam = UGameplayStatics::GetPlayerCameraManager(this, 0);

	if (!IsValid(PlayerCam))
	{
		return;
	}

	FHitResult HitResult;

	FVector Start = PlayerCam->GetCameraLocation();
	FVector End = Start + (PlayerCam->GetCameraRotation().Vector() * 10000.0f);

	FCollisionQueryParams QueryParams;

	TArray<AActor*> IgnoredActors;
	OwningCharacter->GetAttachedActors(IgnoredActors, true);
	QueryParams.AddIgnoredActors(IgnoredActors);
	QueryParams.AddIgnoredActor(GetOwner());

	QueryParams.bTraceComplex = true;
	

	const FName Socket = AimOffsetType == EAimOffsets::Aim ? AimSocketName : LookAtSocketName;

	const bool bHit = GetWorld()->LineTraceSingleByChannel(HitResult, Start, End, TraceChannel, QueryParams);
	if(bHit)
	{
		LookAtLocation = HitResult.ImpactPoint;
		AimOffset = UKismetMathLibrary::FindLookAtRotation(OwningCharacter->GetMesh()->GetSocketLocation(Socket), HitResult.ImpactPoint);
		
	}
	else
	{
		LookAtLocation = HitResult.TraceEnd;
		AimOffset = UKismetMathLibrary::FindLookAtRotation(OwningCharacter->GetMesh()->GetSocketLocation(Socket), HitResult.TraceEnd);
		
	}
}

void UCharacterAnimationComponent::LookAtWithoutCamera()
{
	FHitResult HitResult;
	float OffsetStart = 10000.0f;

	FCollisionQueryParams QueryParams;
	
	QueryParams.bTraceComplex = true;

	TArray<AActor*> IgnoredActors;
	OwningCharacter->GetAttachedActors(IgnoredActors, true);
	QueryParams.AddIgnoredActors(IgnoredActors);
	QueryParams.AddIgnoredActor(GetOwner());

	FName Socket = LookAtSocketName;
	FVector Start = OwningCharacter->GetMesh()->GetSocketLocation(Socket);
	FVector End = (UKismetMathLibrary::GetForwardVector(OwningCharacter->GetController()->GetControlRotation()) * OffsetStart) + Start;

	const bool bHit = GetWorld()->LineTraceSingleByChannel(HitResult, Start, End, TraceChannel, QueryParams);
	if (bHit)
	{
		LookAtLocation = HitResult.ImpactPoint;
		
	}
	else
	{
		LookAtLocation = HitResult.TraceEnd;
		
	}
}

void UCharacterAnimationComponent::OnRep_BasePose(const FGameplayTag OldValue)
{
	OnBasePoseChanged.Broadcast(OldValue, BasePose);
}

void UCharacterAnimationComponent::OnRep_OverlayPose(const FGameplayTag OldValue)
{
	OnOverlayPoseChanged.Broadcast(OldValue, OverlayPose);
}

void UCharacterAnimationComponent::OnRep_RotationMethod()
{
	HandleRotationMethodChange();
}

void UCharacterAnimationComponent::OnRep_RotationSpeed()
{
	HandleRotationSpeedChange();
}



