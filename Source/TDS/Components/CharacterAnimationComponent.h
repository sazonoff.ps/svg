// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayTags.h"
#include "Components/ActorComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "TDS/FuncLibrary/Types.h"
#include "CharacterAnimationComponent.generated.h"



DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnPoseChanged, FGameplayTag, OldValue, FGameplayTag, NewValue);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TDS_API UCharacterAnimationComponent : public UActorComponent
{
	GENERATED_BODY()

public:

	UPROPERTY(BlueprintReadWrite, ReplicatedUsing="OnRep_BasePose", EditAnywhere, Category = "AnimComp|Setup")
	FGameplayTag BasePose;

	UPROPERTY(BlueprintReadWrite, ReplicatedUsing="OnRep_OverlayPose", EditAnywhere, Category = "AnimComp|Setup")
	FGameplayTag OverlayPose;

	UPROPERTY(BlueprintAssignable, Category = "AnimComp|Runtime")
	FOnPoseChanged OnBasePoseChanged;

	UPROPERTY(BlueprintAssignable, Category = "AnimComp|Runtime")
	FOnPoseChanged OnOverlayPoseChanged;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "AnimComp|Runtime")
	FRotator AimOffset;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "AnimComp|Runtime")
	FVector LookAtLocation;

	UPROPERTY(BlueprintReadWrite, Replicated, EditAnywhere, Category = "AnimComp|Setup")
	FGameplayTagContainer AnimModTags;

	UPROPERTY(BlueprintReadWrite, ReplicatedUsing="OnRep_RotationMethod", EditDefaultsOnly, Category = "AnimComp|Setup")
	ERotationMethod RotationMethod = ERotationMethod::None;

	UPROPERTY(BlueprintReadWrite, ReplicatedUsing="OnRep_RotationSpeed", EditDefaultsOnly, Category = "AnimComp|Setup")
	float RotationSpeed = 360.0f;

	UPROPERTY(BlueprintReadWrite, Replicated, EditDefaultsOnly, Category = "AnimComp|Setup")
	float TurnStartAngle = 90.0f;

	UPROPERTY(BlueprintReadWrite, Replicated, EditDefaultsOnly, Category = "AnimComp|Setup")
	float TurnStopTolerance = 1.0f;

	UPROPERTY(BlueprintReadWrite, Replicated, EditDefaultsOnly, Category = "AnimComp|Setup")
	EAimOffsets AimOffsetType = EAimOffsets::None;

	UPROPERTY(BlueprintReadWrite, Replicated, EditDefaultsOnly, Category = "AnimComp|Setup")
	EAimOffsetClamp AimOffsetBehavior = EAimOffsetClamp::Nearest;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "AnimComp|Setup")
	float AimClamp = 135.0f;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "AnimComp|Setup")
	bool CameraBased;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "AnimComp|Setup")
	FName AimSocketName = FName(TEXT("hand_r"));

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "AnimComp|Setup")
	FName LookAtSocketName = FName(TEXT("Spine_03"));

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "AnimComp|Debug")
	TEnumAsByte <ECollisionChannel> TraceChannel = ECollisionChannel::ECC_Visibility;

	UPROPERTY(BlueprintReadOnly, Category = "AnimComp|Components")
	ACharacter* OwningCharacter;

	UPROPERTY(BlueprintReadOnly, Category = "AnimComp|Components")
	UCharacterMovementComponent* OwnerMovementComponent;
	
	UCharacterAnimationComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	
public:	
	// Called every frame
	virtual void TickComponent(const float DeltaTime, const ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	

	UFUNCTION(BlueprintCallable, Category = "AnimComp|Poses")
	void SetupBasePose(FGameplayTag InBasePose);

	UFUNCTION(BlueprintCallable, Category = "AnimComp|Poses")
	void SetupOverlayPose(FGameplayTag InOverlayPose);

	UFUNCTION(BlueprintCallable, Category = "AnimComp|Rotation")
	void SetupRotation(
		const ERotationMethod InRotationMethod = ERotationMethod::None,
		const float InRotationSpeed = 360.0f,
		const float InTurnStartAngle = 90.0f,
		const float InTurnStopTolerance = 5.0f);

	UFUNCTION(BlueprintCallable, Category = "AnimComp|AimOffset")
	void SetupAimOffset(
		const EAimOffsets InAimOffsetType = EAimOffsets::None,
		const EAimOffsetClamp InAimBehavior = EAimOffsetClamp::Nearest,
		const float InAimClamp = 90.0f,
		const bool InCameraBased = true,
		const FName InAimSocketName = FName(TEXT("hand_r")),
		const FName InLookAtSocketName = FName(TEXT("head")));
	

	UFUNCTION(BlueprintCallable, Category = "AnimComp|AnimTags")
	void AddTag(FGameplayTag InTag);

	UFUNCTION(BlueprintCallable, Category = "AnimComp|AnimTags")
	bool RemoveTag(FGameplayTag InTag);

	UFUNCTION(BlueprintCallable, Category = "AnimComp|Tick")
	void AimTick();

	UFUNCTION(BlueprintCallable, Category = "AnimComp|Tick")
	void TurnInPlaceTick();

private:
	
	void RecastOwner();

	void HandleRotationSpeedChange();

	void HandleRotationMethodChange();

	void LookAtIfPlayerControlled();

	void LookAtWithoutCamera();

	UFUNCTION()
	void OnRep_BasePose(const FGameplayTag OldValue);

	UFUNCTION()
	void OnRep_OverlayPose(const FGameplayTag OldValue);

	UFUNCTION()
	void OnRep_RotationMethod();

	UFUNCTION()
	void OnRep_RotationSpeed();
	
};
